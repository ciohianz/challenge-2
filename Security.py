import string
import hashlib
from random import SystemRandom

class Security:

	@staticmethod
	def generatePassword(size):
	    cset = string.ascii_lowercase + string.digits + string.ascii_uppercase
	    password =  ''.join(SystemRandom().choice(cset) for _ in range(size))

	    return password

	@staticmethod
	def generateSalt():
	    cset = string.ascii_lowercase + string.digits + string.ascii_uppercase
	    salt =  ''.join(SystemRandom().choice(cset) for _ in range(128))

	    return salt

	@staticmethod
	def generateHash(salt, password):
		return hashlib.md5( salt + password ).hexdigest()