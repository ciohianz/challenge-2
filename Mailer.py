from __future__ import print_function
import sys
import smtplib

class Mailer:

    def __init__(self, cfgParser):
        self.cfgParser = cfgParser


    def createSMTP(self):
        self.user, passwd, smtp_server = self.cfgParser.get('email', [
                                                        'USER',
                                                        'PASSWD',
                                                        'SMTP_SERVER'
                                                    ])

        self.server = smtplib.SMTP(host=smtp_server, port=587)
        self.server.ehlo()
        self.server.starttls()
        self.server.login(self.user, passwd)

        return self

    def sendEmail(self, to, body, subject):
        message = """From: %s\nTo: %s\nSubject: %s\n\n%s
            """ % (self.user, to, subject, body)


        print("Sending email to user...    ", end="")
        sys.stdout.flush()
        try:
            self.server.sendmail(self.user, to, message)
        except Exception, e:
            print("[ERROR]\n")
            return False
        else:
            print("[DONE]\n")
            return True
