import MySQLdb
import sys

class DBManager:

    def __init__(self, cfgParser):
        self.cfgParser = cfgParser
        host, user, passwd = self.cfgParser.get("database", [
                                        'HOST',
                                        'USER',
                                        'PASSWORD'
                                    ])
        try:
            self.connection = MySQLdb.connect(host=host,
                                        user=user,
                                        passwd=passwd)
            self.cursor = self.connection.cursor()
            
            # for unicode icons
            self.cursor.execute("SET NAMES 'utf8mb4'")
        except Exception, e:
            print "DBManager Error ({}): {}".format(e[0], e[1])
            sys.exit(1)


    def execute(self, stmt):
        return self.cursor.execute(stmt)

    def executePrepared(self, stmt, params):
        return self.cursor.execute(stmt, params)

    def fetchone(self):
        return self.cursor.fetchone()

    def commit(self):
        self.connection.commit()

    def rollback(self):
        self.connection.rollback()

    def close(self):
        self.cursor.close()
        self.connection.close()

    def select_db(self, db):
        self.connection.select_db(db)

    def getLastRowID(self):
        return self.cursor.lastrowid
