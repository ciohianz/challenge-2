import ConfigParser

class ConfigManager:

    def __init__(self):
        self.cfgParser = ConfigParser.ConfigParser()
        self.cfgParser.read('./config.ini')

    def get(self, section, keys):
        if type(keys) == str:
            return self.cfgParser.get(section, keys) 
        values = []
        for key in keys:
            values.append( self.cfgParser.get(section, key) )

        return tuple(values)