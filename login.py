#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ConfigManager import ConfigManager
from DBManager import DBManager
from Security import Security
from ErrorHandler import ErrorHandler

from getpass import getpass
from datetime import datetime, timedelta
import ldap
import ldap.modlist as modlist
import sys

import logging
logging.basicConfig(filename='login.log',level=logging.DEBUG)

# easier way to handle errors
errorHandler = ErrorHandler(logging)


config = ConfigManager()
dbManager = DBManager(config)
WEEKS_IN_MONTH=4

try:
    DB_NAME = config.get('database', 'DB_NAME')
    BASE_DN = config.get('ldap', 'BASE_DN')

    dbManager.select_db( DB_NAME )

    con = ldap.initialize('ldap://' + config.get('ldap', 'HOST'))
except Exception, e:
    msg = "Initial setup failed: {}".format(e)
    errorHandler.handle(msg, exitProgram=True)



def askForCredentials():
    uid = raw_input("UID: ")
    while not uid.strip():
        print "UID cannot be empty"
        uid = raw_input("UID: ")

    password = getpass("Password: ")
    while not password.strip():
        print "Password cannot be empty"
        password = getpass("Password: ")

    return uid, password


def loginWithLDAP(uid, password):
    dn = "uid={},{}".format(uid, BASE_DN)
    try:
        con.simple_bind_s(dn, password)

    except ldap.INVALID_CREDENTIALS, e:
        msg = "Invalid Credentials"
        errorHandler.handle(msg)
        return False

    except Exception, e:
        msg = "LDAP Error: {}".format(e)
        errorHandler.handle(msg)
        return False

    else:
        return True


def getDBUser(uid):
    stmt = "SELECT id, passExpireAt FROM users WHERE ldap_uid=%s"

    try:
        if dbManager.executePrepared( stmt, [uid] ):
            return dbManager.fetchone()
        else:
            raise "User not found!"
    except Exception, e:
        msg = "Error getting user from database: {}".format(e)
        errorHandler.handle(msg, exitProgram=True)


def askForPass():
    while True:
        password = getpass("New password: ")
        while not password.strip():
            print "Password cannot be empty"
            password = getpass("New password: ")

        repassword = getpass("Confirm password: ")
        while not repassword.strip():
            print "Password cannot be empty"
            repassword = getpass("Confirm password: ")

        if password == repassword:
            break
        else:
            print "Passwords dont't match!"

    return password


def updateDBPassword(user_id, sHash, salt):
    stmt = "UPDATE users SET salt=%s, password=%s, passExpireAt=%s WHERE " \
           "users.id=%s"

    try:
        # add two months of difference
        futureExpireAt = datetime.utcnow() + timedelta(weeks=WEEKS_IN_MONTH)
        futureExpireAt = futureExpireAt.strftime('%Y-%m-%d %H:%M:%S')

        dbManager.executePrepared(stmt, [salt, sHash, futureExpireAt, user_id])
    except Exception, e:
        dbManager.rollback()
        msg = "Error updating db user: {}".format(e)
        errorHandler.handle(msg, exitProgram=True)


def updateLDAPUser(uid, oldPassword, newPassword):
    old = {'userPassword':oldPassword}
    new = {'userPassword':newPassword}
    try:
        ldif = modlist.modifyModlist(old,new)

        dn = "uid={},{}".format(uid, BASE_DN)
        con.modify_s(dn, ldif)

    except Exception, e:
        msg = "Error updating ldap user: {}".format(e)
        errorHandler.handle(msg)
        return False

    else:
        return True



def main():
    uid, password = askForCredentials()

    while not loginWithLDAP(uid, password):
        uid, password = askForCredentials()

    print "User authenticated!"

    user = getDBUser(uid)

    user_id, passExpireAt = user
    changePassword = (passExpireAt - datetime.utcnow()).total_seconds() < 0

    if changePassword:
        print "You must change your password!"
        newPass = askForPass()
        newSalt = Security.generateSalt()
        sHash = Security.generateHash(newSalt, newPass)

        updateDBPassword(user_id, sHash, newSalt)

        if updateLDAPUser(uid, password, newPass):
            try:
                # commit password update in MySQL
                dbManager.commit()
                print "Password changed!"
            except Exception, e:
                msg = "Error commiting changes to DB: {}".format(e)
                errorHandler.handle(msg, exitProgram=True)
        else:
            dbManager.rollback()
            msg = "Error updating ldap user"
            errorHandler.handle(msg, exitProgram=True)

    try:
        con.unbind_s()
    except Exception, e:
        msg = "Error logging out user"
        errorHandler.handle(msg, exitProgram=True)




if __name__ == "__main__":
    main()