#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

from ConfigManager import ConfigManager
from Mailer import Mailer
from DBManager import DBManager
from Security import Security
from ErrorHandler import ErrorHandler


import ldap
import ldap.modlist as modlist
import argparse
import sys
import random
import string
import hashlib
from datetime import datetime

import logging
logging.basicConfig(filename='create-users.log',level=logging.DEBUG)

# parsing arguments
parser = argparse.ArgumentParser(description='Create OpenLDAP users')
parser.add_argument('input', type=str,
                    help='CSV file with users to add.')

try:
    parsed_args = parser.parse_args()
except IOError, msg:
    parser.error(str(msg))

# easier way to handle errors
errorHandler = ErrorHandler(logging)

# config manager to read config.ini
config = ConfigManager()

# DBManager to execute database queries
dbManager = DBManager(config)

try:
    # ldap server address
    LDAP_HOST = config.get('ldap', 'HOST')

    # ldap BASE DN
    BASE_DN = config.get('ldap', 'BASE_DN')
except Exception, e:
    msg = "ConfigManager Error: {}".format(e)
    errorHandler.handle(msg, exitProgram=True)

try:
    # smtp server to send emails
    mailer = Mailer(config).createSMTP()
except Exception, e:
    msg = "Mailer Error: {}".format(e)
    errorHandler.handle(msg, exitProgram=True)

def makeLDAPConnection():
    con = None
    try:
        con = ldap.initialize('ldap://' + LDAP_HOST)

        # authenticate admin to create accounts
        username, password = config.get('ldap', [
                                                 'ADMIN_USER',
                                                 'ADMIN_PWD',
                                              ])
        con.simple_bind_s("cn="+username+","+BASE_DN, password)
    except Exception, e:
        msg = "LDAP Error: {}".format(e)
        errorHandler.handle(msg, exitProgram=True)

    return con


def getStateBySKey(sKey):
    stmt = "SELECT id FROM states WHERE sKey = %s"
    if dbManager.executePrepared( stmt, [ sKey ] ):
        return  dbManager.fetchone()[0]
    else:
        return None


def createDBUser(data):
    user_stmt = "INSERT INTO users (email, state_id, passExpireAt, " \
                "password, salt) VALUES ('{}',{},'{}','{}','{}')"

    state_id = getStateBySKey('NEWUSER')
    if state_id:
        email = data['email']
        password = data['password']
        salt = data['salt']
        sHash = Security.generateHash(salt, password)
        passExpAt = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

        try:
            fullStmt = user_stmt.format(email, state_id, passExpAt, sHash, salt)
            dbManager.execute( fullStmt )
            dbManager.commit()

            return dbManager.getLastRowID()
        except Exception, e:
            dbManager.rollback()
            msg = "DBManager Error: {}".format(e)
            errorHandler.handle(msg)
            return False

    else:
        raise "State NEWUSER not found in table states"


def createLDAPUser(data, con):
    # generate random id to avoid equal uids
    randomID = str(random.randint(0, 2000))

    uid = data['firstName'] + data['lastName'] + randomID
    fName = data['firstName']
    lName = data['lastName']
    email = data['email']
    securePassword = data['password']

    modListObject = {
       "objectClass": ["inetOrgPerson", "posixAccount", "shadowAccount"],
       "uid": [uid],
       "sn": [ lName ],
       "givenName": [ fName ],
       "cn": [ fName+" "+lName ],
       "displayName": [ fName+" "+lName ],
       "uidNumber": ["5000"],
       "gidNumber": ["10000"],
       "loginShell": ["/bin/bash"],
       "homeDirectory": ["/home/"+uid],
       "mail": [ email ],
       "userPassword": [ securePassword ]
    }

    # user dn
    dn = 'uid={},{}'.format(uid, BASE_DN)
    
    print("Creating user {} with pass {}...".format(dn, securePassword), end="")
    sys.stdout.flush()

    try:
        result = con.add_s(dn, modlist.addModlist(modListObject))
    except Exception, e:
        msg = "LDAP Error: {}".format(e)
        errorHandler.handle(msg)
        print("    [ERROR]\n")
        return None
    else:
        print("    [DONE]\n")

    createdUser = {
        'dn': dn,
        'passwd': securePassword,
        'email': email,
        'uid': uid
    }

    return createdUser



def readCSV():
    outData = []
    handler = None

    try:
        # open file
        inputFile = parsed_args.input
        handler = open(inputFile, 'r')

        # iterate lines for adding user
        for line in handler.readlines():
            firstName, lastName, email = line.split(',')
            tmp = {
                "firstName": firstName,
                "lastName": lastName,
                "email": email.strip()
            }
            outData.append( tmp )

    except Exception, e:
        if handler:
            handler.close()
        msg = "Error while reading file: {}".format(e)
        errorHandler.handle(msg, exitProgram=True)
        
    if handler:
        handler.close()

    return outData


def bodyTemplate(dn, passwd):
    tpl = "DN: {}\nPassword: {}".format(dn, passwd)

    return tpl


def table_exists(table):
    stmt = ("select * from information_schema.tables where "
            "table_schema = '{}' and table_name = '{}'")

    database = config.get('database', 'DB_NAME')

    return dbManager.execute( stmt.format(database, table) )



def create_users_table():
    stmt = ('CREATE TABLE users('
            'id int(11) unsigned AUTO_INCREMENT,'
            'email char(255) NOT NULL,'
            'ldap_uid char(100) NULL DEFAULT NULL,'
            'state_id int(11) unsigned NOT NULL,'
            'password char(32) NOT NULL,'
            'salt char(128) NOT NULL,'
            'passExpireAt timestamp NOT NULL,'
            'created_at timestamp DEFAULT CURRENT_TIMESTAMP,'
            'primary key (id),'
            'FOREIGN KEY (state_id) REFERENCES states(id),'
            'UNIQUE (email),'
            'UNIQUE (ldap_uid)'
            ');')
    
    dbManager.execute(stmt)
    print("Users table created successfully!")



def create_states_table():
    stmt = ('CREATE TABLE states('
            'id int(11) unsigned AUTO_INCREMENT,'
            'sKey char(7) NOT NULL,'
            'description char(80) NOT NULL,'
            'primary key (id)'
            ') ENGINE=InnoDB;')

    dbManager.execute(stmt)
    print("States table created successfully!")

    # performing states insert
    states = [
        "('NEWUSER', 'Just created user')",
        "('LDRPLOK', 'User replicated into LDAP')",
        "('LDRPLER', 'Error replicating user into LDAP')",
        "('ENOTSEN', 'Email notification sent')",
        "('ENOTERR', 'Email notification error')",
    ]

    insert_stmt = "INSERT INTO states (sKey, description) VALUES "

    dbManager.execute( insert_stmt + (',').join(states) )


def check_tables():
    database = config.get('database', 'DB_NAME')

    try:
        dbManager.select_db(database)
    except Exception, e:
        try:
            stmt = "CREATE DATABASE {};".format(database)
            dbManager.execute(stmt)
            dbManager.select_db(database)
        except Exception, e:
            raise e

    if not table_exists('states'):
        create_states_table()

    if not table_exists('users'):
        create_users_table()


def updateState(user_id, stateKey):
    state_id = getStateBySKey(stateKey)

    if not state_id:
        raise "State %s not found in table states" % stateKey

    stmt = "UPDATE users SET state_id = {} WHERE users.id = {}"

    try:
        dbManager.execute( stmt.format(state_id, user_id) )
        dbManager.commit()
    except Exception, e:
        dbManager.rollback()
        msg = "DBManager Error: {}".format(e)
        errorHandler.handle(msg)


def updateUserUID(user_id, uid):
    stmt = "UPDATE users SET ldap_uid=%s WHERE users.id=%s"

    try:
        dbManager.executePrepared(stmt, [ uid, user_id ])
        dbManager.commit()
    except Exception, e:
        dbManager.rollback()
        msg = "DBManager Error: {}".format(e)
        errorHandler.handle(msg)


def main():

    # check if tables exist
    try:
        check_tables()
    except Exception, e:
        msg = "DBManager Error: {}".format(e)
        errorHandler.handle(msg, exitProgram=True)

    # create ldap connection
    ldapConnection = makeLDAPConnection()

    # read and parse users data from csv
    usersData = readCSV()

    # itearete users data to persist users in ldap
    if len( usersData ):
        for userData in usersData:
            try:

                # generate ldap password for current user
                userData['password'] = Security.generatePassword(9)
                userData['salt'] = Security.generateSalt()

                # create mysql user if not exists
                user_id = createDBUser(userData)

                if user_id:
                    # make ldap request to create user
                    user = createLDAPUser(userData, ldapConnection)
                    if user:
                        updateState(user_id, 'LDRPLOK')

                        updateUserUID(user_id, user['uid'])

                        body = bodyTemplate( user['dn'], user['passwd'] )
                        subject = 'Credenciales LDAP'


                        if mailer.sendEmail(user['email'], body, subject):
                            updateState(user_id, 'ENOTSEN')
                        else:
                            msg = "Error sending email " \
                                  "to: {}".format(userData['email'])
                            errorHandler.handle(msg)
                            
                            updateState(user_id, 'ENOTERR')

                    else:
                        updateState(user_id, 'LDRPLER')
                        print("Error creating user {} in " \
                              "ldap".format(userData['email']))
                else:
                    print("Error creating user {} in " \
                          "database".format(userData['email']))
                    

                print("\n\n")

            except Exception, e:
                if mailer.server:
                    mailer.server.close()
                msg = "Error creating user: {}".format(e)
                errorHandler.handle(msg)

            if mailer.server:
                mailer.server.close()


if __name__ == "__main__":
    main()

